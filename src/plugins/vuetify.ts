import 'material-design-icons-iconfont/dist/material-design-icons.css';
import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi', // default - only for display purposes
  },
  theme: {
    themes: {
      light: {
        primary: '#DE7E00',
        secondary: '#673ab7',
        accent: '#03a9f4',
        error: '#ff5722',
        info: '#ffc107',
        success: '#2196f3',
        warning: '#8bc34a',
      },
    },
  },
});
